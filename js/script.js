$(document).ready(function () {
    var $el = $('header');
    $(window).scroll(function () {
        if ($(this).scrollTop() > 10) {
            $el.addClass('sticky');
        } else {
            $el.removeClass('sticky');
        }
    });

    if (window.matchMedia('(min-width: 767px)').matches) {
        $(".search").on("click", function () {
            $("#for_search").css("display", "inline-block").animate({opacity: 1}, 500);
            $(".search_line").css("display", "flex");
            $("#for_search_pic").css("display", "inline-block");
            $("nav").css("display", "none");
            $(".search").css("display", "none");
            $(".logo").css("margin-right", "43px");
            $(".freeze_bg").css("display", "block");
        });

        $(".freeze_bg").on("click", function () {
            $("#for_search").css("display", "none").animate({opacity: 0}, 1000);
            $(".search_line").css("display", "none!important");
            $("#for_search_pic").css("display", "none");
            $("nav").css("display", "block");
            $(".search").css("display", "block");
            $(".logo").css("margin-right", "63px");
            $(".freeze_bg").css("display", "none");
        });
    }


    $('.urgent').parents('.mid_news_block').addClass('urgent_hover');
    $('.timeline_news_urgent').parents('.timeline_news_block').addClass('urgent_hover');

    $(".burger").on("click", function () {
        $(".burger_block").addClass("burger_active");
        $("body").css("overflow", "hidden");

    });

    $(".burger_close").on("click", function () {
        $(".burger_block").addClass('burger_close_sc');
        $('.burger_block').removeClass('burger_active');
        $("body").css("overflow", "auto");

    });


    let x = $(".subMenus_mobile_plus");
    $(x).bind('click', function (e) {
        $(e.target).parent(".menus_mobile").children(".subNavMenu_mobile").toggle();
        $(e.target).toggleClass("subMenus_mobile_minus");
    });
    $('.generalNewsSlider').slick({
        arrows: false,
        dots: true,
        autoplay: true,
    });
    if (window.matchMedia('(max-width: 1024px)').matches) {
        $('.video_sec').slick({
            arrows: false,
            dots: true,
            autoplay: true,

        });
        $('.same_posts').slick({
            arrows: false,
            dots: true,
            autoplay: true,

        });
    } else {
        console.log("err")
    }

    if (window.matchMedia('(max-width: 680px)').matches) {
        $(".search").on("click", function () {
            $(".modal_search_block").addClass("modal_search_block_active");
            $("body").css("overflow", "hidden")
        });

        $(".close_modal").on("click", function () {
            $(".modal_search_block").removeClass("modal_search_block_active");
            $(".modal_search_block").addClass("modal_search_block_close");
            $("body").css("overflow", "auto")


        });
    }


});





